/***************************************************************************/
/*                                                                         */
/* ifsplot - a program to generate the attractor of an IFS.                */
/* Copyright (C) 2014, 2003 Ricardo Biloti <biloti@ime.unicamp.br>         */
/*                    http://www.ime.unicamp.br/~biloti                    */ 
/*                                                                         */
/* This program is free software; you can redistribute it and/or modify    */
/* it under the terms of the GNU General Public License as published by    */
/* the Free Software Foundation; either version 2, or (at your option)     */
/* any later version.                                                      */
/*                                                                         */
/* This program is distributed in the hope that it will be useful,         */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/* GNU General Public License for more details.                            */
/*                                                                         */
/* You should have received a copy of the GNU General Public License       */
/* along with this program; if not, write to the                           */
/* Free Software Foundation, Inc., 59 Temple Place - Suite 330,            */
/* Boston, MA 02111-1307, USA.                                             */
/*                                                                         */
/***************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <plot.h>
#include "cmdline.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef _SQR
#define _SQR 1
#define sqr(x)           ( ((x)*(x)) )
#endif

#ifndef _MAX
#define _MAX 1
#define max(x,y)         ( ((x) < (y)) ? (y) : (x) )
#endif

#ifndef _MIN
#define _MIN 1
#define min(x,y)         ( ((x) > (y)) ? (y) : (x) )
#endif

#define DOT 1

unsigned short ifspoint(float **ifs, float *x, float *y){

   float p, xnew;
   unsigned short k;
   
   p = rand()/(1.0* RAND_MAX);
   
   k=0;
   while (p>=ifs[k][6]){
      k++;
   }

   xnew = ifs[k][0]* (*x) + ifs[k][1]* (*y) + ifs[k][4];
   *y   = ifs[k][2]* (*x) + ifs[k][3]* (*y) + ifs[k][5];
   *x   = xnew;
   
   return k;
}

void center(unsigned short N, float **ifs, float *x, float *y){

   float delta;

   *x = 0;
   *y = 0;

   for(unsigned short i=0; i<N; i++){
      delta = (1-ifs[i][0])*(1-ifs[i][3])-ifs[i][1]*ifs[i][2];
      *x += (ifs[i][1]*ifs[i][5] + ifs[i][4]*(1-ifs[i][3]))/delta;
      *y += (ifs[i][2]*ifs[i][4] + ifs[i][5]*(1-ifs[i][0]))/delta;
   }

   *x = *x/N;
   *y = *y/N;
}

float radius(unsigned short N, float **ifs, float x, float y){

   float s, alpha, beta, gamma, ri, radius;
   float x1, y1;

   radius = 0;
   
   for(unsigned short i=0; i<N; i++){
      alpha = (sqr(ifs[i][0])+sqr(ifs[i][2]))/2;
      beta =  (sqr(ifs[i][1])+sqr(ifs[i][3]))/2;
      gamma = ifs[i][0]*ifs[i][1] + ifs[i][2]*ifs[i][3];

      s = sqrt(alpha + beta + sqrt( sqr(alpha-beta) + sqr(gamma)));

      x1 = ifs[i][0]*x + ifs[i][1]*y + ifs[i][4];
      y1 = ifs[i][2]*x + ifs[i][3]*y + ifs[i][5];

      ri = sqrt(sqr(x-x1) + sqr(y-y1))/(1-s);
      radius = max(radius, ri);
   }

   return radius;
}

int main(int argc, char **argv){

   int handle;
   float **ifs, x, y, rad;
   float xi, xf, yi, yf;
   float xmax, xmin, ymax, ymin;
   unsigned short N;
   unsigned short nmax;
   size_t n, npre;
   
   int nc = 13;
   int color[][3]={{    0,65535,    0},	  /* green             */     
		   {65535,65535,    0},	  /* yellow            */     
		   {    0,65535,65535},	  /* cyan              */     
		   {65535,    0,    0},	  /* red               */     
		   {65535,65535,65535},   /* white             */
		   {65535,42405,    0},	  /* orange            */     
		   {65535,    0,65535},	  /* magenta           */     
		   {    0,    0,65535},	  /* blue              */     
		   {65535,65535,57344},   /* light yellow      */
		   {57568,65535,65535},	  /* light cyan        */     
		   {29298, 8481,48316},	  /* indigo            */     
		   {16448,57568,53456},	  /* turquoise         */
		   {60928,16384,    0}};  /* orange red 2      */


   struct gengetopt_args_info arg;

   if (cmdline_parser(argc, argv, &arg) != 0)
      exit(EXIT_FAILURE);

   n = 2 << arg.maxint_arg;
   npre = 2 << arg.minint_arg;
   nmax = (unsigned short) arg.nmax_arg;

   fprintf(stderr, "Number of points: %zi\n", n);
   fprintf(stderr, "Number of iterations to skip: %zi\n", npre);

   if (!arg.color_flag) nc=1;

   /* Allocate space for the IFS transformations */
   ifs  = (float **) malloc(sizeof(float *) * nmax);
   for (unsigned short i=0; i<nmax; i++)
      ifs[i]  = (float *) malloc(sizeof(float) * 7);

   /* Load the affine transformations */
   N = 0;
   while ((fscanf(stdin,"%f %f %f %f %f %f %f", 
                  &ifs[N][0], &ifs[N][1], &ifs[N][2], &ifs[N][3],
                  &ifs[N][4], &ifs[N][5], &ifs[N][6]) == 7) &&
          (N < nmax)){
     if (N > 0)
       ifs[N][6] += ifs[N-1][6];
     
     N++;
   }
   ifs[N-1][6] = 1;

   /* Pre iterations */
   /* Start from a point that certainly belongs to the attractor */
   /* This is the fixed point of the first transformation */
   center(N, ifs, &x, &y);
   rad = radius(N, ifs, x, y);

   fprintf(stderr,"Guess center: (%5.3f , %5.3f), radius = %4.2f\n",
           x, y, rad);
   fprintf(stderr,"Guess region: [% 5.3f,% 5.3f]x[% 5.3f,% 5.3f]\n",
           x-rad, x+rad, y-rad, y+rad);
   
   xmax = x;
   xmin = x;
   ymax = y;
   ymin = y;

   for (size_t i=0;i<npre;i++){
      ifspoint(ifs, &x, &y);
      xmax = max(x, xmax);
      xmin = min(x, xmin);
      ymax = max(y, ymax);
      ymin = min(y, ymin);
   }

   if (arg.xi_given)  xi = arg.xi_arg;
   else xi = xmin - (xmax-xmin)/20;

   if (arg.xf_given) xf = arg.xf_arg;
   else xf = xmax + (xmax-xmin)/20;

   if (arg.yi_given) yi = arg.yi_arg;
   else yi = ymin - (ymax-ymin)/20;

   if (arg.yf_given) yf = arg.yf_arg;
   else yf = ymax + (ymax-ymin)/20;


   pl_parampl ("PAGESIZE", "a4");
   /* pl_parampl ("BITMAPSIZE", "750x750"); */

   if ((handle = pl_newpl (arg.driver_arg, stdin, stdout, stderr)) < 0){
      fprintf (stderr, "Couldn't create Plotter\n");
      return EXIT_FAILURE;
   }
   pl_selectpl (handle);

   if (pl_openpl () < 0){
     fprintf (stderr, "Couldn't open Plotter\n");
     return EXIT_FAILURE;
   }

   pl_fspace (xi,yi,xf,yf);

   if (strncmp(arg.driver_arg, "X", 1) != 0){
      nc = 1;
      color[0][0]=0;
      color[0][1]=0;
      color[0][2]=0;
      pl_bgcolorname ("white");
   }
   else{
      pl_bgcolorname("black");
   }
   pl_erase ();

   {
     if (strncmp(arg.driver_arg, "ps", 1) == 0){
       pl_flinewidth (0.25);
       
       for (size_t i=0;i<n;i++){
         register div_t d;
         int ic;
         ic = ifspoint(ifs, &x, &y);
         
         d = div(ic, nc);
         pl_color(color[d.rem][0],color[d.rem][1],color[d.rem][2]);
         
         pl_fmarker (x, y, DOT, 0.05);
         
         xmax = max(x, xmax);
         xmin = min(x, xmin);
         ymax = max(y, ymax);
         ymin = min(y, ymin);
       }
     }
     else{
       for (size_t i=0;i<n;i++){
         register div_t d;
         int ic;
         ic = ifspoint(ifs, &x, &y);
         
         d = div(ic, nc);
         pl_color(color[d.rem][0],color[d.rem][1],color[d.rem][2]);
         
         pl_fpoint (x, y);
         
         xmax = max(x, xmax);
         xmin = min(x, xmin);
         ymax = max(y, ymax);
         ymin = min(y, ymin);
       }
     }
   }

   fprintf(stderr,"Points inside [% 5.3f,% 5.3f]x[% 5.3f,% 5.3f]\n",
	   xmin, xmax, ymin, ymax);
   
   if (pl_closepl () < 0){
      fprintf (stderr, "Couldn't close Plotter\n");
      return EXIT_FAILURE;
   }

   pl_selectpl (0);
   if (pl_deletepl (handle) < 0){
      fprintf (stderr, "Couldn't delete Plotter\n");
      return EXIT_FAILURE;
   }

   /* Free ifs */
   for (unsigned short i=0; i<nmax; i++)
      free(ifs[i]);
   free(ifs);
   
   cmdline_parser_free(&arg);

   return EXIT_SUCCESS;
}
